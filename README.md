# DISSECT

DISSECT was designed for running on distributed environment systems. In this regard, it uses an implementation of ScaLAPACK libraries and some of the available MPI libraries which are present in nearly all supercomputers. However, this does not prevent DISSECT to run on single computers with smaller datasets provided that these libraries are present.

DISSECT is developed in Roslin Institute.

For the original documentation, please visit DISSECT home page at http://www.dissect.ed.ac.uk

## Changes in this version

- Compatibility with newer compilers (tested with GCC-9, GCC-10 and GCC-11)
- Selectable Scalapack's routines for faster PCA analysis across hardware configurations.

## Performance optimisation

### PCA
The computation time of PCA analysis (`--pca`) can vary depending on sample size and computing hardware configuration.
Changing the eigensolver implementation can substantially improve performance. 
Three implementations are supported and can be selected with this version by passing the `--eigsolver <0|1|2>` argument.

0: QR method (default),  
1: Divide-and-conquer (DD) method,  
2: Relatively Robust Representations (RR) method

References:  
https://netlib.org/lapack/lawnspdf/lawn183.pdf  
https://link.springer.com/chapter/10.1007/978-3-642-55224-3_3


### GWAS
Dissect can run GWAS analysis (`--gwas`) in parallel and serial.
The parallelization is enabled by default. To disable it, add `--nonparallel-gwas` as an argument.

Dissect mainly uses MPI to parallelize `--gwas` through SCALAPACK. However, some functions (e.g., filling missing values) are parallelized using OpenMP, and Dissect assumes that each MPI process uses the same number of threads returned by the `omp_get_num_threads()` function. As a result, running Dissect with more than one MPI processes per node could degrade performance caused by thread oversubscription.  

There are three ways as workaround:

1. Run it with one MPI process per node (total number of MPI processes = number of nodes). In this mode, each MPI process uses all the cores of one node without oversubscribing them.

2. Disable multithreading by setting OMP_NUM_THREADS = 1 (recommended). This means cores will only be used by MPI processes.
3. Specify OpenMP threads for each MPI process to ensure each CPU core is allocated for only one thread.  
In MPICH and Intel MPI, we can pass OMP_NUM_THREADS environment variable as mpirun arguments.  
`$ mpirun -np <num_procs> -genv OMP_NUM_THREADS=<num_threads_per_proc> ./dissect --gwas --bfile ..`

For example, suppose we have two nodes with 16 CPU cores per node (32 cores in total), and we want to use 2 MPI processes per node (4 processes in total) without thread oversubscription. 

`$ mpirun -np 4 -genv OMP_NUM_THREADS=8 ./dissect --gwas --bfile ..`

(The total number of threads spawned = 4 * 8 = 32, which is equal to the total number of CPU cores)

#### Setting OpenMP threads in PBS (e.g., [NURION](https://docs-ksc.gitbook.io/nurion-user-guide-eng/system/job-execution-through-scheduler-pbs) supercomputer)
The number of OpenMP threads can be set via the resource option (`ompthreads` variable) in the PBS job submission script.

## Acknowledgment
This project was supported by [British Council Reconnect Travel Grant](https://opportunities-insight.britishcouncil.org/news/opportunities/research-opportunity-hong-kong-japan-korea-and-uk-reconnect-travel-grant-%E2%80%93-open).  

The author would like to thank Dr. Hyojung Paik and Dr. Oh Kyoung Kwon of [KISTI](https://www.kisti.re.kr/eng/) for access to NURION and valuable discussions.


